#! /bin/bash

#Version: 1
# Date:- 20/07/2022

currentDir=$(dirname ${BASH_SOURCE[0]})
baseDir=$(dirname $currentDir)


test1(){
	(echo "test@gmail.com";echo "n";echo "test@gmail.com";echo "y";echo "123@#2";echo "123@#2") | bash $baseDir/install.sh
}
test2(){
	(echo "test@gmail.com";echo "n";echo "test@gmail.com";echo "y";echo "123@#2";echo "123@ 2";echo "123@#2";echo "123@#2") | bash $baseDir/install.sh
}
test3(){
	(echo "test@gmail.com";echo "n";echo "test@gmail.com";echo "n";echo "123@#2";echo "123@ 2";echo "123@#2";echo "123@#2") | bash $baseDir/install.sh
}

execute(){
	# Execute test moduele
	$1
	bat $baseDir/src/pass_file_template
}


execute "test3"
#execute "test2"
