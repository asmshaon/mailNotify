#!/usr/bin/python3

#Version: 2
#14/07/2022 - Thursday - 05:41 PM

import imaplib
import email
import datetime
from os import system
from password import gmail1, App_Pass

# if there is any new unread emails from today this variable will increment
# each unread emails of today will increment by 1
new_mail = 0

############ Current Date ##################################
# To check how many mails came today; today's dete must be known
# ctime(): gives the full information of date
# ctime(): makes dateToday into a string from datetime.date format
# split(): to make the string into a list
dateToday = datetime.date.today().ctime().split()

# Date String arranging block:
# Gmail date format is: day, date month year time GMT -> Thu, 14 Jul 2022 07:38:00 GMT
# Python date time is: day month date time  year -> Thu Jul 14 00:00:00 2022
#
#
# so, i need to make sure that my local date list follows the same pattern. Time & GMT are not needed.
# so, pop the day to add ',' at the end
# reindex the rest to match placement
day = dateToday.pop(0)
dateToday.insert(0, day+",")
mon = dateToday.pop(2)
dateToday.insert(1, mon)
year = dateToday.pop(-1)
dateToday.insert(-1, year)
# Outcome: The the first 4 elements of both date lists(one from computer, one from gmail) will be in same pattern

#print(" String: {}".format(dateToday))

############### IMAP4 SSL: establish connection ##############################

# using IMAP4_SSL as a contex manager
# Standard port for IMAP4_SSL is 993
with imaplib.IMAP4_SSL(host="imap.gmail.com", port=imaplib.IMAP4_SSL_PORT) as imap_ssl:
    print(f"Connection Object: {imap_ssl}")

    ############### Logging into gmail ###################################
    print(f"Logging into mailbox......")
    resp_code, response = imap_ssl.login(gmail1, App_Pass)

    print(f"Response Code :\t{resp_code}")
    print(f"Response      :\t{response[0].decode()}")


    ############### Set MailBox ###################################
    # After this command all operations will be performed in selected mailbox
    # Setting readonly boolean value to False will mark the fetched mails as read
    # the returned tuple contains 2 values
    # We don't need either one of them
    resp_code, mail_count = imap_ssl.select(mailbox="INBOX", readonly=True)

    ############### Retrieve Mail IDs for given directory ################
    resp_code, mails = imap_ssl.search(None, "UnSeen")
    # returned values needs to be decoded
    # then split() method will be used to turn it into a list

    ################### Fetching Dates and sender #########################
    # decode() & split() only works on string
    # mails is a list obeject with length of 1 which is accessed by mails[0]
    # as mails[0] entity is string, it can be manipulated using string methods
    # Without decode() output of mails[0].split() will look like this:
    #           [b'393', b'395', b'396', b'397', b'399' , ...]

    for mail_id in mails[0].decode().split()[-4:]:
        print(f"================== Start of Mail [{mail_id}] ====================")
        resp_code, mail_data = imap_ssl.fetch(mail_id, '(RFC822)') # Fetch mail data; RFC822-> Full data
        message = email.message_from_bytes(mail_data[0][1]) # Construct Message from mail data

        ################## Date Matching ################################
        matched = 0
        mail_date = message.get("Date").split()

        for i in range(4):
            if mail_date[i] != dateToday[i]:
                print(f"didn't match: {mail_date[i]}")
                matched = 1
                break
            else:
                print(f"matched: {mail_date[i]}")

        if matched == 0:
            new_mail += 1
            print(f"Date matched")
        elif matched == 1:
            print(f"Date didn't matched")


        print(f"================== End of Mail [{mail_id}] ====================\n")

    ############# Close Selected Mailbox #######################
    print(f"\nClosing selected mailbox....")
    imap_ssl.close()


print(f"New UnSeen Mail: {new_mail}")

############################## Sending notification ##############################
#  Ddependency: notify-send
#               A notification daemon i.e. Dunst (used in my machine)

if new_mail > 0:
    #system("export DISPLAY=:0")

    # CronJob doesn't get runtime evn variables
    # So this needs to be provided
    system(f'XDG_RUNTIME_DIR=/run/user/$(id -u) /usr/bin/notify-send "Gmail" "{new_mail} UnRead email"')
