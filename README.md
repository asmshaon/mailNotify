# mailNotify

A python script to send desktop notification after detecting new email.
Only works for gmail account.

## File Tree

```txt
mailNotify/
├── cronjob-setup
├── install.sh
├── README.md
├── src
│   ├── mailNotify.py
│   └── pass_file_template
└── test
    ├── pass_file_backup
    └── test.sh

```

## Requirement

- Python 3.6
- bash 4.4
- git
- cron

## Platform

It only works on:

	- linux
	- gmail

## Installation

### Using git

This repo needs to be cloned into local directory and execute [install](https://gitlab.com/asmshaon/mailnotify/-/blob/master/install.sh).

using https:

```bash
git clone https://gitlab.com/asmshaon/mailnotify ~/
~/mailnotify/./install.sh
```
or using ssh:
```sh
git clone git@gitlab.com:asmshaon/mailnotify.git ~/
~/mailnotify/./install.sh
```

### Modification

The script holds some default value that can be changed based on user preference
These variables have "# Changable" written right above them, individually.

* Installation dir : /opt/mailNotify
* auto\_cron: 0 (change the value in range of 1-9, single digit)
* attempt: 2 (How many retries will be allowed)

##### CronJob:

* Current cronjob will execute the script every 2 hours
* modification to the rule should be done on the file: [cronjob-setup](https://gitlab.com/asmshaon/mailnotify/-/blob/master/cronjob-setup)


## Use case

Keeping email client open consumes too much resource.
Now, these can be put on standby, only to be refreshed after being notified.

## TODO

1. More email services will be added in the future
2. add Oauth authentication for better security
3. New one using Gmail API

* Note:
	Both No. 2 & 3 needs me to use Google cloud services. Because google just have to make everything more difficult then it needs to be.

## Resources

* [imaplib - Simple Guide to Manage Mailboxes using Python](https://coderzcolumn.com/tutorials/python/imaplib-simple-guide-to-manage-mailboxes-using-python)
* [cron job helper](https://cron.help/)
